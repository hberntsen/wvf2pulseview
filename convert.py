#!/usr/bin/env python3

import wvfreader
import numpy as np
import sys

if len(sys.argv) != 3:
    print("Usage: convert.py input.WVF output.bin")
    sys.exit(1)

df = wvfreader.DataFile(sys.argv[1], True)
df.info()

sample_time = df.traces[0].attrs['x_gain']
for trace in df.traces.values():
    assert trace.attrs['x_gain'] == sample_time

# Concatenates all blocks of each channel
bindata = np.transpose([trace.get_data()[1] for trace in df.traces.values()], (2, 1, 0))
print('Output shape:', bindata.shape)
print('Output format:', bindata.dtype, '(Probably Little endian?)')
print('Number of channels:', len(df.traces))
print('Sample rate:', 1/sample_time,'hz')

bindata.tofile(sys.argv[2])
